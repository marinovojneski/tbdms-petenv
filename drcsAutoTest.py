import logging
from time import sleep
import pytest_check as check

from logging import *

from epics import caget, caput
from myLib import scale
from opc_client import OPCClient

from deviceClasses import *

# Get devices from a list /csv/
sleepval = 0.5
myFile = open("myData.csv", "r")

logger = logging.getLogger()

myPT = [" "]
myCV = [" "]
myYSV = [" "]
myP = [" "]
myKSB = [" "]

for aline in myFile:
    vals = aline.split(",")

    valX = vals[1]
    if valX == "INI":
        myTestInit = Init(vals[2], vals[3], vals[4])

    if valX in ["PT","PDT","PIT","FT" ,"FIT","TT","QT","QIT","LT" ,"LIT"]:
        myPT.append(Analog(vals[2], vals[3], vals[4], float(vals[5]), float(vals[6]), float(vals[7]), float(
            vals[8]), float(vals[9]), float(vals[10]), float(vals[11]), float(vals[12]), float(vals[13]), float(vals[14]), float(vals[15]), vals[16]))

    if valX == ("FCV" or "TCV" or "PCV" or "LCV"):
        myCV.append(CV(vals[2], vals[3], vals[4], vals[5]))

    if valX == "YSV":
        myYSV.append(ON_OFF_VALVE(vals[2], vals[3], vals[4], vals[5], vals[6]))

    if valX == "P":
        myP.append(PUMP(vals[2], vals[3], vals[4], vals[5]))

    if valX == "SICA":
        myKSB.append(KSB_PUMP(vals[2], vals[3], vals[4]))

myFile.close()

# Opc client

ioc_ip = myTestInit.IocIp
plc_ip = myTestInit.PlcIp

client = OPCClient(plc_ip)

# Test cases

# PT tests

#   test_LevelMeasurements_1 contains alarm check for MV alarming setup structure.
def test_LevelMeasurements_1():
    i = 1
    while i < len(myPT):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myPT[i].Tag)
        print("=======================================================================================================")
        if (myPT[i].Tag == "1043_PIT_118"):
            a = 1
        sclerrormsg = "@ " + myPT[i].Tag + \
            " Wrong scaling configuration in the PLC!"
        alarmerrormsg = "@ " + myPT[i].Tag + \
            " Alarm triggered but not at the correct Value!"

        # init
        caput(myPT[i].PvName+":P_Limit_HIHI", myPT[i].ValHiHi)
        caput(myPT[i].PvName+":P_Limit_HI", myPT[i].ValHi)
        caput(myPT[i].PvName+":P_Limit_LO", myPT[i].ValLo)
        caput(myPT[i].PvName+":P_Limit_LOLO", myPT[i].ValLoLo)
        caput(myPT[i].PvName + ":P_AlarmSetpoint", myPT[i].setpoint)

        client.connect()

        # ---- Get nodes ----
        measValNode = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myPT[i].OpcInNode])

        sleep(sleepval)
        # ------------------- Checks -------------------
        # Scale check
        scaleValues = [myPT[i].ChkVal1, myPT[i].ChkVal2, myPT[i].ChkVal3, myPT[i].ChkVal4]
        for sv in scaleValues:
            print(myPT[i].Tag + ":Scale check")
            # Round sv on 2 decimals
            roundTestValue = float("{:.2f}".format(sv))

            print(myPT[i].Tag + ": Set " + str(roundTestValue) + " to " + myPT[i].OpcInNode)
            OPCClient.setValue(client, measValNode, scale(roundTestValue, myPT[i].RngLo, myPT[i].RngHi))
            sleep(0.5)
            epicsPv = myPT[i].PvName+":"+myPT[i].physicalVariable
            # Round sv on 2 decimals
            roundEpicsValue = float("{:.2f}".format(caget(epicsPv)))
            print(myPT[i].Tag + ": Check " + str(epicsPv) + " = " + myPT[i].OpcInNode + ": " + str(roundEpicsValue) + " = " + str(sv))
            check.equal(roundTestValue, roundEpicsValue, sclerrormsg)
            sleep(0.5)

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before LOW alarms checks")
        # Check alarm Absolute/Relative
        absoluteAlarm = caget(myPT[i].PvName + ":" + "FB_Absolute")
        if (absoluteAlarm == 0):
            alarmSetpoint = caget(myPT[i].PvName + ":" + "FB_AlarmSetpoint")
            loloLimit = alarmSetpoint - (myPT[i].ValLoLo / 100) * alarmSetpoint
            loLimit = alarmSetpoint - (myPT[i].ValLo / 100) * alarmSetpoint
            hiLimit = alarmSetpoint + (myPT[i].ValHi / 100) * alarmSetpoint
            hihiLimit = alarmSetpoint + (myPT[i].ValHiHi / 100) * alarmSetpoint
        else:
            alarmSetpoint = (myPT[i].ValHi - myPT[i].ValLo) / 2
            loloLimit = myPT[i].ValLoLo
            loLimit = myPT[i].ValLo
            hiLimit = myPT[i].ValHi
            hihiLimit = myPT[i].ValHiHi

        OPCClient.setValue(client, measValNode, scale(alarmSetpoint, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        # Set alarm delay to 0
        alarmDelay = caget(myPT[i].PvName + ":" + "FB_Delay")
        caput(myPT[i].PvName + ":" + "P_Delay", 0)

        # Alarm checks
        # Low alarms

        if ((myPT[i].ValLo != 0) or (myPT[i].ValLoLo != 0)):
            print(myPT[i].Tag + ": Low Alarms")
            loPrint = 0
            loloPrint = 0

            simVal = alarmSetpoint

            #   This is to get optimal decrement step
            min1 = min(abs(alarmSetpoint - abs(loLimit)), abs(loLimit - abs(loloLimit)))
            simInc = 0.95 * min1

            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Decrement = " + str(simInc))

            #   Go down starting from simVal = alarmSetpoint with decrement steps simInc
            while simVal > myPT[i].RngLo:
                OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":LO") == 1:
                    epicsValueLo = caget(epicsPv)
                    check.less_equal(epicsValueLo, loLimit, alarmerrormsg)
                    if (loPrint == 0):
                        print(myPT[i].Tag + ": LO alarm at " + epicsPv + " = " + str(epicsValueLo) + " with simVal = " + str(simVal) + ". LO_Limit = " + str(myPT[i].ValLo) + "/" + str(loLimit))
                        loPrint = 1
                if caget(myPT[i].PvName+":LOLO") == 1:
                    epicsValue = caget(epicsPv)
                    check.less_equal(epicsValue, loloLimit, alarmerrormsg)
                    if (loloPrint == 0):
                        print(myPT[i].Tag + ": LOLO alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". LOLO_Limit = " + str(myPT[i].ValLoLo) + "/" + str(loloLimit))
                        loloPrint = 1
                    #   Exit loop if LOLO reached
                    if (epicsValue <= loloLimit):
                        break
                #   Decrement simVal
                simVal -= simInc
        else:
            print(myPT[i].Tag + ": There are NO Low Alarms")

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before HIGH alarms checks")

        simVal = alarmSetpoint

        OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        if ((myPT[i].ValHi != 0) or (myPT[i].ValHiHi != 0)):
            # High alarms
            print(myPT[i].Tag + ": High Alarms")
            hiPrint = 0
            hihiPrint = 0

            simVal = alarmSetpoint
            #   This is to get optimal increment step
            min1 = min(abs(hiLimit - alarmSetpoint), abs(hihiLimit - hiLimit))
            simInc = 0.95 * min1
            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Increment = " + str(simInc))

            #   Go up starting from simVal = alarmSetpoint with increment steps simInc
            while simVal < myPT[i].RngHi:
                OPCClient.setValue(client, measValNode, scale(
                    simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":HI") == 1:
                    epicsValueHi = caget(epicsPv)
                    check.greater_equal(epicsValueHi, hiLimit, alarmerrormsg)
                    if (hiPrint == 0):
                        print(myPT[i].Tag + ": HI alarm at " + epicsPv + " = " + str(epicsValueHi) + " with simVal = " + str(simVal) + ". HI_Limit = " + str(myPT[i].ValHi) + "/" + str(hiLimit))
                        hiPrint = 1
                        sleep(sleepval)
                if caget(myPT[i].PvName+":HIHI") == 1:
                    epicsValue = caget(epicsPv)
                    check.greater_equal(epicsValue, hihiLimit, alarmerrormsg)
                    if (hihiPrint == 0):
                        print(myPT[i].Tag + ": HIHI alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". HI_Limit = " + str(myPT[i].ValHiHi) + "/" + str(hihiLimit))
                        hihiPrint = 1
                        sleep(sleepval)
                    #   Exit loop if HIHI reached
                    if (epicsValue >= hihiLimit):
                        break
                #   Increment simVal
                simVal += simInc
        else:
            print(myPT[i].Tag + ": There are NO High Alarms")

        # Return alarm delay
        caput(myPT[i].PvName + ":" + "P_Delay", alarmDelay)

        client.disconnect()

        i += 1

def test_LevelMeasurements():
    i = 1
    while i < len(myPT):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myPT[i].Tag)
        print("=======================================================================================================")
        if (myPT[i].Tag == "1043_PIT_118"):
            a = 1
        sclerrormsg = "@ " + myPT[i].Tag + \
            " Wrong scaling configuration in the PLC!"
        alarmerrormsg = "@ " + myPT[i].Tag + \
            " Alarm triggered but not at the correct Value!"

        # init
        caput(myPT[i].PvName + ":P_Limit_HIHI", myPT[i].ValHiHiHi)
        caput(myPT[i].PvName+":P_Limit_HIHI", myPT[i].ValHiHi)
        caput(myPT[i].PvName+":P_Limit_HI", myPT[i].ValHi)
        caput(myPT[i].PvName+":P_Limit_LO", myPT[i].ValLo)
        caput(myPT[i].PvName+":P_Limit_LOLO", myPT[i].ValLoLo)
        caput(myPT[i].PvName + ":P_AlarmSetpoint", myPT[i].setpoint)

        client.connect()

        # ---- Get nodes ----
        measValNode = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myPT[i].OpcInNode])

        sleep(sleepval)
        # ------------------- Checks -------------------
        # Scale check
        scaleValues = [myPT[i].ChkVal1, myPT[i].ChkVal2, myPT[i].ChkVal3, myPT[i].ChkVal4]
        for sv in scaleValues:
            print(myPT[i].Tag + ":Scale check")
            # Round sv on 2 decimals
            roundTestValue = float("{:.2f}".format(sv))

            print(myPT[i].Tag + ": Set " + str(roundTestValue) + " to " + myPT[i].OpcInNode)
            OPCClient.setValue(client, measValNode, scale(roundTestValue, myPT[i].RngLo, myPT[i].RngHi))
            sleep(0.5)
            epicsPv = myPT[i].PvName+":"+myPT[i].physicalVariable
            # Round sv on 2 decimals
            roundEpicsValue = float("{:.2f}".format(caget(epicsPv)))
            print(myPT[i].Tag + ": Check " + str(epicsPv) + " = " + myPT[i].OpcInNode + ": " + str(roundEpicsValue) + " = " + str(sv))
            check.equal(roundTestValue, roundEpicsValue, sclerrormsg)
            sleep(0.5)

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before LOW alarms checks")
        # Check alarm Absolute/Relative
        alarmSetpoint = (myPT[i].ValHi - myPT[i].ValLo) / 2
        loloLimit = myPT[i].ValLoLo
        loLimit = myPT[i].ValLo
        hiLimit = myPT[i].ValHi
        hihiLimit = myPT[i].ValHiHi
        hihihiLimit = myPT[i].ValHiHiHi

        OPCClient.setValue(client, measValNode, scale(alarmSetpoint, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        # Alarm checks
        # Low alarms

        if ((myPT[i].ValLo != 0) or (myPT[i].ValLoLo != 0)):
            print(myPT[i].Tag + ": Low Alarms")
            loPrint = 0
            loloPrint = 0

            simVal = alarmSetpoint

            #   This is to get optimal decrement step
            min1 = min(abs(alarmSetpoint - abs(loLimit)), abs(loLimit - abs(loloLimit)))
            simInc = 0.95 * min1

            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Decrement = " + str(simInc))

            #   Go down starting from simVal = alarmSetpoint with decrement steps simInc
            while simVal > myPT[i].RngLo:
                OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":LO") == 1:
                    epicsValueLo = caget(epicsPv)
                    check.less_equal(epicsValueLo, loLimit, alarmerrormsg)
                    if (loPrint == 0):
                        print(myPT[i].Tag + ": LO alarm at " + epicsPv + " = " + str(epicsValueLo) + " with simVal = " + str(simVal) + ". LO_Limit = " + str(myPT[i].ValLo) + "/" + str(loLimit))
                        loPrint = 1
                if caget(myPT[i].PvName+":LOLO") == 1:
                    epicsValue = caget(epicsPv)
                    check.less_equal(epicsValue, loloLimit, alarmerrormsg)
                    if (loloPrint == 0):
                        print(myPT[i].Tag + ": LOLO alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". LOLO_Limit = " + str(myPT[i].ValLoLo) + "/" + str(loloLimit))
                        loloPrint = 1
                    #   Exit loop if LOLO reached
                    if (epicsValue <= loloLimit):
                        break
                #   Decrement simVal
                simVal -= simInc
        else:
            print(myPT[i].Tag + ": There are NO Low Alarms")

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before HIGH alarms checks")

        simVal = alarmSetpoint

        OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        if ((myPT[i].ValHi != 0) or (myPT[i].ValHiHi != 0)):
            # High alarms
            print(myPT[i].Tag + ": High Alarms")
            hiPrint = 0
            hihiPrint = 0
            hihihiPrint = 0

            simVal = alarmSetpoint
            #   This is to get optimal increment step
            min1 = min(abs(hiLimit - alarmSetpoint), abs(hihihiLimit - hihiLimit))
            simInc = 0.95 * min1
            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Increment = " + str(simInc))

            #   Go up starting from simVal = alarmSetpoint with increment steps simInc
            while simVal < myPT[i].RngHi:
                OPCClient.setValue(client, measValNode, scale(
                    simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":HI") == 1:
                    epicsValueHi = caget(epicsPv)
                    check.greater_equal(epicsValueHi, hiLimit, alarmerrormsg)
                    if (hiPrint == 0):
                        print(myPT[i].Tag + ": HI alarm at " + epicsPv + " = " + str(epicsValueHi) + " with simVal = " + str(simVal) + ". HI_Limit = " + str(myPT[i].ValHi) + "/" + str(hiLimit))
                        hiPrint = 1
                        sleep(sleepval)
                if caget(myPT[i].PvName+":HIHI") == 1:
                    epicsValue = caget(epicsPv)
                    check.greater_equal(epicsValue, hihiLimit, alarmerrormsg)
                    if (hihiPrint == 0):
                        print(myPT[i].Tag + ": HIHI alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". HIHI_Limit = " + str(myPT[i].ValHiHi) + "/" + str(hihiLimit))
                        hihiPrint = 1
                        sleep(sleepval)
                if caget(myPT[i].PvName+":HIHIHI") == 1:
                    epicsValue = caget(epicsPv)
                    check.greater_equal(epicsValue, hihiLimit, alarmerrormsg)
                    if (hihihiPrint == 0):
                        print(myPT[i].Tag + ": HIHIHI alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". HIHIHI_Limit = " + str(myPT[i].ValHiHiHi) + "/" + str(hihihiLimit))
                        hihihiPrint = 1
                        sleep(sleepval)
                    #   Exit loop if HIHI reached
                    if (epicsValue >= hihiLimit):
                        break
                #   Increment simVal
                simVal += simInc
        else:
            print(myPT[i].Tag + ": There are NO High Alarms")

        client.disconnect()

        i += 1

def test_ControlValves():
    i = 1
    while i < len(myCV):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myCV[i].Tag)
        print("=======================================================================================================")
        errormsg = "@ " + myCV[i].Tag + " Cant't set the specified value!"
        # Connect to the PLC
        client.connect()

        CvSp = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myCV[i].SetPoint])

        CvFb = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myCV[i].FeedBack])

        # Check SPs and FBs and zeroing
        valveValues = [0, 25, 50, 75, 100]
        for vv in valveValues:
            epicsSetpoint = myCV[i].PvName+":P_Setpoint"
            print(myCV[i].Tag + ": Set " + epicsSetpoint + " = " + str(vv))
            caput(epicsSetpoint, vv)
            sleep(1)
            # Return Setpoint to the Feedback
            retVal = OPCClient.getValue(client, CvSp)
            OPCClient.setValue(client, CvFb, retVal)

            sleep(1)
            epicsPositionFeedback = myCV[i].PvName+":ValvePosition"
            epicsPositionFeedbackValue = caget(epicsPositionFeedback)

            valveFeedback = scale(vv)
            print(myCV[i].Tag + ": Check " + epicsPositionFeedback + " = " + myCV[i].FeedBack + ":   " + str(epicsPositionFeedbackValue) + " = " + str(valveFeedback))
            check.is_true(OPCClient.getValue(client, CvSp) == scale(vv), errormsg)

            epicsPositionCommand = myCV[i].PvName + ":FB_Manipulated"
            epicsPositionCommandValue = caget(epicsPositionCommand)
            print(myCV[i].Tag + ": Check " + epicsPositionCommand + " = " + myCV[i].SetPoint + ":   " + str(epicsPositionCommandValue) + " = " + str(valveFeedback))
            check.is_true(epicsPositionCommandValue == vv, errormsg)

            sleep(sleepval)

        # # Set secound value (100%)
        # caput(myCV[i].PvName+":P_Setpoint", 100)
        # sleep(sleepval)
        # # Return Setpoint to the Feedback
        # retVal = OPCClient.getValue(client, CvSp)
        #
        # sleep(sleepval)
        # OPCClient.setValue(client, CvFb, retVal)
        # sleep(sleepval)
        # # Check value
        # check.is_true(OPCClient.getValue(client, CvSp) == scale(100), errormsg)
        # check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 100, errormsg)

        # OPC client disconnect
        client.disconnect()

        i += 1

def test_OnOffValves():
    i = 1
    while i < len(myYSV):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myYSV[i].Tag)
        print("=======================================================================================================")
        errormsg1 = "@ " + myYSV[i].Tag + " Cant't control the valve!"
        errormsg2 = "@ " + myYSV[i].Tag + " Valve did not open!"
        errormsg3 = "@ " + myYSV[i].Tag + " Valve did not close!"
        errormsg4 = "@ " + myYSV[i].Tag + " Both feedback signal is active!"
        # Connect to the PLC
        client.connect()

        YsvCommand = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myYSV[i].Command])
        YsvFbOpen = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myYSV[i].FeedBack_open])
        YsvFbClose = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myYSV[i].FeedBack_close])

        # Check Open state
        cmdOpenPvName = myYSV[i].PvName+":Cmd_ManuOpen"
        print(myYSV[i].Tag + ": Set " + cmdOpenPvName + " = " + str(1))
        caput(cmdOpenPvName, True)
        sleep(sleepval)

        retVal = OPCClient.getValue(client, YsvCommand)
        print(myYSV[i].Tag + ": Get value " + myYSV[i].Command + " = " + str(retVal))
        sleep(sleepval)

        if retVal == True:
            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_open + " = " + str(retVal))
            OPCClient.setValue(client, YsvFbOpen, retVal)

            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_close + " = " + str(not(retVal)))
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        else:
            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_open + " = " + str(retVal))
            OPCClient.setValue(client, YsvFbOpen, retVal)

            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_close + " = " + str(not (retVal)))
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        sleep(sleepval)

        ysvCommandPLCValue = OPCClient.getValue(client, YsvCommand)
        print(myYSV[i].Tag + ": Check " + myYSV[i].Command + " = " + str(1) + ": " + str(ysvCommandPLCValue) + " = " + str(1))
        check.is_true(ysvCommandPLCValue == 1, errormsg1)

        ysvOpenedPvName = myYSV[i].PvName+":Opened"
        ysvOpenedPvValue = caget(ysvOpenedPvName)
        print(myYSV[i].Tag + ": Check " + ysvOpenedPvName + " = " + str(1) + ": " + str(ysvOpenedPvValue) + " = " + str(1))
        check.is_true(ysvOpenedPvValue == 1, errormsg2)
        sleep(sleepval)

        ysvClosedPvName = myYSV[i].PvName + ":Closed"
        ysvClosedPvValue = caget(ysvClosedPvName)
        print(myYSV[i].Tag + ": Check false " + ysvClosedPvName + " = " + ysvOpenedPvName + ": " + str(ysvClosedPvValue) + " = " + str(ysvOpenedPvValue))
        check.is_false(ysvClosedPvValue == ysvOpenedPvValue, errormsg4)
        sleep(sleepval)

        # Check Close state
        cmdClosePvName = myYSV[i].PvName + ":Cmd_ManuClose"
        print(myYSV[i].Tag + ": Set " + cmdClosePvName + " = " + str(1))
        caput(cmdClosePvName, True)
        sleep(sleepval)

        retVal = OPCClient.getValue(client, YsvCommand)
        print(myYSV[i].Tag + ": Get value " + myYSV[i].Command + " = " + str(retVal))
        sleep(sleepval)

        if retVal == True:
            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_open + " = " + str(retVal))
            OPCClient.setValue(client, YsvFbOpen, retVal)

            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_close + " = " + str(not (retVal)))
            OPCClient.setValue(client, YsvFbClose, not (retVal))
        else:
            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_open + " = " + str(retVal))
            OPCClient.setValue(client, YsvFbOpen, retVal)

            print(myYSV[i].Tag + ": Set " + myYSV[i].FeedBack_close + " = " + str(not (retVal)))
            OPCClient.setValue(client, YsvFbClose, not (retVal))
        sleep(sleepval)

        ysvCommandPLCValue = OPCClient.getValue(client, YsvCommand)
        print(
            myYSV[i].Tag + ": Check " + myYSV[i].Command + " = " + str(0) + ": " + str(ysvCommandPLCValue) + " = " + str(
                0))
        check.is_true(OPCClient.getValue(client, YsvCommand) == 0, errormsg1)

        ysvClosedPvValue = caget(ysvClosedPvName)
        print(
            myYSV[i].Tag + ": Check " + ysvClosedPvName + " = " + str(1) + ": " + str(ysvClosedPvValue) + " = " + str(1))
        check.is_true(caget(myYSV[i].PvName+":Closed") == 1, errormsg3)
        sleep(sleepval)

        ysvOpenedPvValue = caget(ysvOpenedPvName)
        print(myYSV[i].Tag + ": Check false " + ysvClosedPvName + " = " + ysvOpenedPvName + ": " + str(
            ysvClosedPvValue) + " = " + str(ysvOpenedPvValue))
        check.is_false(ysvClosedPvValue == ysvOpenedPvValue, errormsg4)


        # OPC client disconnect
        client.disconnect()

        i += 1


# def test_Pumps():
#     i = 1
#     while i < len(myP):
#         print(" ")
#         print(" ")
#         print("=======================================================================================================")
#         print(myKSB[i].Tag)
#         print("=======================================================================================================")
#         errormsg1 = "@ " + myYSV[i].Tag + " Cant't control the pump!"
#         errormsg2 = "@ " + myYSV[i].Tag + " Cant't start the pump!"
#         errormsg3 = "@ " + myYSV[i].Tag + " Cant't stop the pump!"
#
#         # Connect to the PLC
#         client.connect()
#
#         P_Command = client.get_root_node().get_child(
#             ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myP[i].Control])
#         P_Status = client.get_root_node().get_child(
#             ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myP[i].Status])
#
#
#         # Check Pump running
#         caput(myP[i].PvName+":Cmd_ManuStart", True)
#         sleep(sleepval)
#         retVal = OPCClient.getValue(client, P_Command)
#         sleep(sleepval)
#         OPCClient.setValue(client, P_Status, retVal)
#         sleep(sleepval)
#         check.is_true(OPCClient.getValue(client, P_Command) == 1, errormsg1)
#         check.is_true(caget(myP[i].PvName+":Running") == 1, errormsg2)
#         check.is_true(caget(myP[i].PvName+":Stopped") == 0, errormsg2)
#
#
#         sleep(sleepval)
#
#         # Check Pump stoped
#         caput(myP[i].PvName+":Cmd_ManuStop", True)
#         sleep(sleepval)
#         retVal = OPCClient.getValue(client, P_Command)
#         sleep(sleepval)
#         OPCClient.setValue(client, P_Status, retVal)
#         sleep(sleepval)
#         check.is_true(OPCClient.getValue(client, P_Command) == 0, errormsg1)
#         check.is_true(caget(myP[i].PvName+":Stopped") == 1, errormsg3)
#         check.is_true(caget(myP[i].PvName+":Running") == 0, errormsg3)
#
#
#         # OPC client disconnect
#         client.disconnect()
#
#         i += 1


def test_KSB_Pumps():
    i = 1
    while i < len(myKSB):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myKSB[i].Tag)
        print("=======================================================================================================")
        errormsg = "@ " + myKSB[i].Tag + " Cant't set the specified value!"
        # Connect to the PLC
        client.connect()

        systemNo = myKSB[i].Tag.split("_")[0]

        KsbSp = client.get_root_node().get_child(
            ['0:Objects', '3:' + myTestInit.OpcName, '3:DataBlocksGlobal', '3:' + systemNo + '_KSB_Pumps_IO', '3:' + myKSB[i].Pump, '3:udtKSB_RDP_WR',
         '3:iOutSetpoint'])

        KsbFb = client.get_root_node().get_child(
            ['0:Objects', '3:' + myTestInit.OpcName, '3:DataBlocksGlobal', '3:' + systemNo + '_KSB_Pumps_IO', '3:' + myKSB[i].Pump, '3:udtKSB_DM',
         '3:rInSpeed'])

        # Check SPs and FBs and zeroing
        pumpValues = [0, 25, 50, 75, 100]
        for vv in pumpValues:
            epicsSetpoint = myKSB[i].PvName + ":P_Setpoint"
            print(myKSB[i].Tag + ": Set " + epicsSetpoint + " = " + str(vv))
            caput(epicsSetpoint, vv)
            sleep(1)
            # Return Setpoint to the Feedback
            retVal = OPCClient.getValue(client, KsbSp)
            OPCClient.setValue(client, KsbFb, retVal)

            sleep(1)
            epicsPositionFeedback = myKSB[i].PvName + ":PumpActSpeed"
            epicsPositionFeedbackValue = caget(epicsPositionFeedback)

            pumpFeedback = scale(vv)
            print(myKSB[i].Tag + ": Check " + epicsPositionFeedback + " = " + myKSB[i].Pump + "udtKSB_DM.udtKSB_DM.rInSpeed" + ":   " + str(
                epicsPositionFeedbackValue) + " = " + str(pumpFeedback))
            check.is_true(OPCClient.getValue(client, KsbSp) == scale(vv), errormsg)

            epicsPositionCommand = myKSB[i].PvName + ":FB_Setpoint"
            epicsPositionCommandValue = caget(epicsPositionCommand)
            print(myKSB[i].Tag + ": Check " + epicsPositionCommand + " = " + myKSB[i].Pump + "udtKSB_RDP_WR.udtKSB_RDP_WR.iOutSetpoint" + ":   " + str(
                epicsPositionCommandValue) + " = " + str(pumpFeedback))
            check.is_true(epicsPositionCommandValue == vv, errormsg)

            sleep(sleepval)

        #// Map Drive EPICS Parameters
        #IF(  # iStKSB_RDPRDRetVal = 0) THEN
            # EPICS.Ready := #KSB.udtKSB_RDP_RD.bInReady;
            # EPICS.Alarm := #KSB.udtKSB_RDP_RD.bInFault;
            # EPICS.RemoteControlReq := #KSB.udtKSB_RDP_RD.bInBus_Control_Mode;
            # EPICS.MaxSpeedReached := #KSB.udtKSB_RDP_RD.bInPumpSpeedMax;
            #END_IF;

        # EPICS.PumpActSpeed := #KSB.udtKSB_DM.rInSpeed;
        # EPICS.PumpOUTFreq := #KSB.udtKSB_DM.rInOutputFrequency;
        # EPICS.PumpOUTVoltage := #KSB.udtKSB_DM.rInMotorVoltage;
        # EPICS.PumpOUTCurrent := #KSB.udtKSB_DM.rInMotorCurrent;
        # EPICS.PumpOUTPower := #KSB.udtKSB_DM.rInMotorInputPower;
        # EPICS.PumpPUTemp := #KSB.udtKSB_DM.rInHeatSinkTemperature;

        # # Set secound value (100%)
        # caput(myKSB[i].PvName+":P_Setpoint", 100)
        # sleep(sleepval)
        # # Return Setpoint to the Feedback
        # retVal = OPCClient.getValue(client, KsbSp)
        #
        # sleep(sleepval)
        # OPCClient.setValue(client, KsbFb, retVal)
        # sleep(sleepval)
        # # Check value
        # check.is_true(OPCClient.getValue(client, KsbSp) == scale(100), errormsg)
        # check.is_true(caget(myKSB[i].PvName+":FB_Manipulated") == 100, errormsg)

        # OPC client disconnect
        client.disconnect()

        i += 1

def main():
    pass


if __name__ == '__main__':
    main()
