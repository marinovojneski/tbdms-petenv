import logging
from time import sleep
import pytest_check as check

from logging import *

from epics import caget, caput
from myLib import scale
from opc_client import OPCClient

from deviceClasses import *

# Get devices from a list /csv/
sleepval = 0.5
myFile = open("myData.csv", "r")

logger = logging.getLogger()

myPT = [" "]
myCV = [" "]
myYSV = [" "]
myP = [" "]
myKSB = [" "]

for aline in myFile:
    vals = aline.split(",")

    valX = vals[1]
    if valX == "INI":
        myTestInit = Init(vals[2], vals[3], vals[4])

    if valX in ["PT","PDT","PIT","FT" ,"FIT","TT","TE","QT","QIT","LT" ,"LIT"]:
        myPT.append(Analog(vals[2], vals[3], vals[4], float(vals[5]), float(vals[6]), float(vals[7]), float(
            vals[8]), float(vals[9]), float(vals[10]), float(vals[11]), float(vals[12]), float(vals[13]), float(vals[14]), float(vals[15]), float(vals[16]), vals[17]))

    if valX == ("FCV" or "TCV" or "PCV" or "LCV"):
        myCV.append(CV(vals[2], vals[3], vals[4], vals[5]))

    if valX == "YSV":
        myYSV.append(ON_OFF_VALVE(vals[2], vals[3], vals[4], vals[5], vals[6]))

    if valX == "P":
        myP.append(PUMP(vals[2], vals[3], vals[4], vals[5]))

    if valX == "SICA":
        myKSB.append(KSB_PUMP(vals[2], vals[3], vals[4]))

myFile.close()

# Opc client

ioc_ip = myTestInit.IocIp
plc_ip = myTestInit.PlcIp

client = OPCClient(plc_ip)

# Test cases

# Temperature tests
def test_TemperatureMeasurements():
    i = 1
    while i < len(myPT):
        print(" ")
        print(" ")
        print("=======================================================================================================")
        print(myPT[i].Tag)
        print("=======================================================================================================")
        if (myPT[i].Tag == "1043_PIT_118"):
            a = 1
        sclerrormsg = "@ " + myPT[i].Tag + \
            " Wrong scaling configuration in the PLC!"
        alarmerrormsg = "@ " + myPT[i].Tag + \
            " Alarm triggered but not at the correct Value!"

        # init
        caput(myPT[i].PvName + ":P_Limit_HIHIHI", myPT[i].ValHiHiHi)
        caput(myPT[i].PvName+":P_Limit_HIHI", myPT[i].ValHiHi)
        caput(myPT[i].PvName+":P_Limit_HI", myPT[i].ValHi)
        caput(myPT[i].PvName+":P_Limit_LO", myPT[i].ValLo)
        caput(myPT[i].PvName+":P_Limit_LOLO", myPT[i].ValLoLo)
        #   caput(myPT[i].PvName + ":P_AlarmSetpoint", myPT[i].setpoint)

        client.connect()

        # ---- Get nodes ----
        measValNode = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myPT[i].OpcInNode])

        sleep(sleepval)
        # ------------------- Checks -------------------
        # Scale check
        scaleValues = [myPT[i].ChkVal1, myPT[i].ChkVal2, myPT[i].ChkVal3, myPT[i].ChkVal4]
        for sv in scaleValues:
            print(myPT[i].Tag + ":Scale check")
            # Round sv on 2 decimals
            roundTestValue = float("{:.2f}".format(sv))

            print(myPT[i].Tag + ": Set " + str(roundTestValue) + " to " + myPT[i].OpcInNode)
            OPCClient.setValue(client, measValNode, scale(roundTestValue, myPT[i].RngLo, myPT[i].RngHi))
            sleep(0.5)
            epicsPv = myPT[i].PvName+":"+myPT[i].physicalVariable
            # Round sv on 2 decimals
            roundEpicsValue = float("{:.2f}".format(caget(epicsPv)))
            print(myPT[i].Tag + ": Check " + str(epicsPv) + " = " + myPT[i].OpcInNode + ": " + str(roundEpicsValue) + " = " + str(sv))
            check.equal(roundTestValue, roundEpicsValue, sclerrormsg)
            sleep(0.5)

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before LOW alarms checks")
        # Check alarm Absolute/Relative
        alarmSetpoint = (myPT[i].ValHi - myPT[i].ValLo) / 2
        loloLimit = myPT[i].ValLoLo
        loLimit = myPT[i].ValLo
        hiLimit = myPT[i].ValHi
        hihiLimit = myPT[i].ValHiHi
        hihihiLimit = myPT[i].ValHiHiHi

        OPCClient.setValue(client, measValNode, scale(alarmSetpoint, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        # Alarm checks
        # Low alarms

        if ((myPT[i].ValLo != 0) or (myPT[i].ValLoLo != 0)):
            print(myPT[i].Tag + ": Low Alarms")
            loPrint = 0
            loloPrint = 0

            simVal = alarmSetpoint

            #   This is to get optimal decrement step
            min1 = min(abs(alarmSetpoint - abs(loLimit)), abs(loLimit - abs(loloLimit)))
            simInc = 0.95 * min1

            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Decrement = " + str(simInc))

            #   Go down starting from simVal = alarmSetpoint with decrement steps simInc
            while simVal > myPT[i].RngLo:
                OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":LO") == 1:
                    epicsValueLo = caget(epicsPv)
                    check.less_equal(epicsValueLo, loLimit, alarmerrormsg)
                    if (loPrint == 0):
                        print(myPT[i].Tag + ": LO alarm at " + epicsPv + " = " + str(epicsValueLo) + " with simVal = " + str(simVal) + ". LO_Limit = " + str(myPT[i].ValLo) + "/" + str(loLimit))
                        loPrint = 1
                if caget(myPT[i].PvName+":LOLO") == 1:
                    epicsValue = caget(epicsPv)
                    check.less_equal(epicsValue, loloLimit, alarmerrormsg)
                    if (loloPrint == 0):
                        print(myPT[i].Tag + ": LOLO alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". LOLO_Limit = " + str(myPT[i].ValLoLo) + "/" + str(loloLimit))
                        loloPrint = 1
                    #   Exit loop if LOLO reached
                    if (epicsValue <= loloLimit):
                        break
                #   Decrement simVal
                simVal -= simInc
        else:
            print(myPT[i].Tag + ": There are NO Low Alarms")

        # Ack alarms
        print(myPT[i].Tag + ": Acknowledge eventual alarms before HIGH alarms checks")

        simVal = alarmSetpoint

        OPCClient.setValue(client, measValNode, scale(simVal, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)

        if ((myPT[i].ValHi != 0) or (myPT[i].ValHiHi != 0)):
            # High alarms
            print(myPT[i].Tag + ": High Alarms")
            hiPrint = 0
            hihiPrint = 0
            hihihiPrint = 0

            simVal = alarmSetpoint
            #   This is to get optimal increment step
            min1 = min(abs(hiLimit - alarmSetpoint), abs(hihihiLimit - hihiLimit))
            simInc = 0.95 * min1
            print(myPT[i].Tag + ": Set " + str(epicsPv) + " = " + str(simVal) + "; Increment = " + str(simInc))

            #   Go up starting from simVal = alarmSetpoint with increment steps simInc
            while simVal < myPT[i].RngHi:
                OPCClient.setValue(client, measValNode, scale(
                    simVal, myPT[i].RngLo, myPT[i].RngHi))
                sleep(sleepval)
                if caget(myPT[i].PvName+":HI") == 1:
                    epicsValueHi = caget(epicsPv)
                    check.greater_equal(epicsValueHi, hiLimit, alarmerrormsg)
                    if (hiPrint == 0):
                        print(myPT[i].Tag + ": HI alarm at " + epicsPv + " = " + str(epicsValueHi) + " with simVal = " + str(simVal) + ". HI_Limit = " + str(myPT[i].ValHi) + "/" + str(hiLimit))
                        hiPrint = 1
                        sleep(sleepval)
                if caget(myPT[i].PvName+":HIHI") == 1:
                    epicsValue = caget(epicsPv)
                    check.greater_equal(epicsValue, hihiLimit, alarmerrormsg)
                    if (hihiPrint == 0):
                        print(myPT[i].Tag + ": HIHI alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". HIHI_Limit = " + str(myPT[i].ValHiHi) + "/" + str(hihiLimit))
                        hihiPrint = 1
                        sleep(sleepval)
                if caget(myPT[i].PvName+":HIHIHI") == 1:
                    epicsValue = caget(epicsPv)
                    check.greater_equal(epicsValue, hihiLimit, alarmerrormsg)
                    if (hihihiPrint == 0):
                        print(myPT[i].Tag + ": HIHIHI alarm at " + epicsPv + " = " + str(epicsValue) + " with simVal = " + str(simVal) + ". HIHIHI_Limit = " + str(myPT[i].ValHiHiHi) + "/" + str(hihihiLimit))
                        hihihiPrint = 1
                        sleep(sleepval)
                    #   Exit loop if HIHI reached
                    if (epicsValue >= hihiLimit):
                        break
                #   Increment simVal
                simVal += simInc
        else:
            print(myPT[i].Tag + ": There are NO High Alarms")

        client.disconnect()

        i += 1

def main():
    pass


if __name__ == '__main__':
    main()
