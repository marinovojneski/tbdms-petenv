
# ---------------------------------------------------------------------------- #
#                 Device types for DRCS system simulator.                      #
#               The simulator using opcua for PLC communication.               #
#             Created by Peter Talas | peter.talas@evopro-group.com            #
#                                 2020.June.04.                                #
# ---------------------------------------------------------------------------- #

# ---------------------------------- Imports --------------------------------- #

from logging import error
from time import sleep
from opc_client import OPCClient
from myLib import scale, biScale

# ------------------------------- Device types: ------------------------------ #

class OnOffValve():
    """
    For on-off or PV valves.
    """

    def __init__(self, client, plcName, opened, closed, qOpen):
        self.client = client
        self.plcName = plcName
        self.qOpen = qOpen
        self.opened = opened
        self.closed = closed

    def SIM(self):
        '''
        Simulationg opening and closing function.
        '''
        valveOpened = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.opened])
        valveClosed = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.closed])
        valveOpen = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.qOpen])
        # Simulation
        if (OPCClient.getValue(self.client, valveOpen)) == True:
            OPCClient.setValue(self.client, valveClosed, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveOpened, True)
            return "Open"
        else:
            OPCClient.setValue(self.client, valveOpened, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveClosed, True)
            return "Close"
# ---------------------------------------------------------------------------- #

class ControlValve():
    """
    For on-off or CV valves.
    """

    def __init__(self, client, plcName, pv, sp):
        self.client = client
        self.plcName = plcName
        self.sp = sp
        self.pv = pv

    def SIM(self):
        '''
        Simulationg opening and closing function.
        '''
        valvePv = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.pv])

        valveSp = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.sp])
            
        # Simulation
        valveSpValue = (OPCClient.getValue(self.client, valveSp))
        if valveSpValue >= 0:
            OPCClient.setValue(self.client, valvePv, valveSpValue)
            #sleep(0.001)
        else:
            print('CV valve setpoint error')
        return valveSpValue
# ---------------------------------------------------------------------------- #


def AnalogDevice(client, plcName, valuePath, rawValue):
    valueNode = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', valuePath])
    if rawValue <= 27648:
        OPCClient.setValue(client, valueNode, rawValue)
    else:
        OPCClient.setValue(client, valueNode, 27648)
    if rawValue >= 0:
        OPCClient.setValue(client, valueNode, rawValue)
    else:
        OPCClient.setValue(client, valueNode, 0)
    return rawValue
# ---------------------------------------------------------------------------- #

class DiscretePump():
    def __init__(self, client, plcName, valuePath):
      self.client = client
      self.plcName = plcName
      self.valuePath = valuePath
    def SIM(self):
        valueNode = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.valuePath])

        
        if (OPCClient.getValue(self.client, valueNode) == (True or 1)):
            return True
        else:
            return False

# ---------------------------------------------------------------------------- #
