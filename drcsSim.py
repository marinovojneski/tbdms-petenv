from DeviceTypes import *
from opc_client import OPCClient
from myLib import scale


#Constants
ip = "192.168.0.223"
plcName = '3:DRCS_PLC001'

#Connect to PLC
client = OPCClient(ip)
client.connect()


#Devices
LIA101_1047 = AnalogDevice
LIA111_1047 = AnalogDevice
LIA121_1047 = AnalogDevice
PIT115_1047 = AnalogDevice
PIT125_1047 = AnalogDevice
YSV200 = OnOffValve(client, plcName, '3:1047_YSV_200_PosOpen', '3:1047_YSV_200_PosClose', '3:1047_YSV_200_CmdOpCo')
YSV110 = OnOffValve(client, plcName, '3:1047_YSV_110_PosOpen', '3:1047_YSV_110_PosClose', '3:1047_YSV_110_CmdOpCo')
YSV210 = OnOffValve(client, plcName, '3:1047_YSV_210_PosOpen', '3:1047_YSV_210_PosClose', '3:1047_YSV_210_CmdOpCo')
YSV100 = OnOffValve(client, plcName, '3:1047_YSV_100_PosOpen', '3:1047_YSV_100_PosClose', '3:1047_YSV_100_CmdOpCo')
YSV220 = OnOffValve(client, plcName, '3:1047_YSV_220_PosOpen', '3:1047_YSV_220_PosClose', '3:1047_YSV_220_CmdOpCo')
FCV160 = ControlValve(client, plcName, '3:1047_HIC_160_PosFB', '3:1047_HIC_160_CmdPos')
FCV170 = ControlValve(client, plcName, '3:1047_HIC_170_PosFB', '3:1047_HIC_170_CmdPos')
FCV240 = ControlValve(client, plcName, '3:1047_HIC_240_PosFB', '3:1047_HIC_240_CmdPos')
FCV120 = ControlValve(client, plcName, '3:1047_HIC_120_PosFB', '3:1047_HIC_120_CmdPos')
FCV180 = ControlValve(client, plcName, '3:1047_HIC_180_PosFB', '3:1047_HIC_180_CmdPos')
P012 = DiscretePump(client, plcName, '3:1047_P_012_CmdRun')
P013 = DiscretePump(client, plcName, '3:1047_P_013_CmdRun')

#Static values
LIA101_1047_value = scale(50)
LIA111_1047_value = scale(50)
LIA121_1047_value = scale(50)
PIT115_1047_value = scale(50)
PIT125_1047_value = scale(50)

#SIMulation main cycle

while (KeyboardInterrupt):
    #Main
    LIA101_1047(client, plcName, '3:1047_PIT_101_TReading', LIA101_1047_value)
    LIA111_1047(client, plcName, '3:1047_PIT_111_TReading', LIA101_1047_value)
    LIA121_1047(client, plcName, '3:1047_PDIT_121_TReading', LIA101_1047_value)
    PIT115_1047(client, plcName, '3:1047_PIT_115_TReading', LIA101_1047_value)
    PIT125_1047(client, plcName, '3:1047_PIT_125_TReading', LIA101_1047_value)
    
    YSV200.SIM()
    YSV110.SIM()
    YSV210.SIM()
    YSV100.SIM()
    YSV220.SIM()
    FCV160.SIM()
    FCV170.SIM()
    FCV240.SIM()
    FCV120.SIM()
    FCV180.SIM()

    P012.SIM()
    P013.SIM()

    print('Simulation Running!')

client.disconnect()
print('SIM aborted!')