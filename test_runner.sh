#!/bin/sh
now=`date +%Y%m%d_%H%M%S`
pytest -v drcsAutoTest.py --self-contained-html --html=./html/DRCSautotest_report_$now.html
